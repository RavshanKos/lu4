#pragma once
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GUARDL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// ZPORT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifndef ZPORT_LINKONREQUEST

#ifdef ZPORT_EXPORTS
#define ZPORT_API(type) extern "C" __declspec(dllexport) type __stdcall 
#else
#ifdef ZPORT_STATIC
#define ZPORT_API(type) extern "C" type __stdcall 
#else
#define ZPORT_API(type) extern "C" __declspec(dllimport) type __stdcall 
#endif
#endif
#endif

#include <windows.h>

#define ZP_SDK_VER_MAJOR	1
#define ZP_SDK_VER_MINOR	10

#pragma pack(1)   // turn byte alignment on

typedef INT ZP_STATUS;	// ��������� ���������� ������� DLL

#define ZP_SUCCESS						0	// �������� ��������� �������
#define ZP_E_CANCELLED					1	// �������� �������������
#define ZP_E_NOT_FOUND					2	// �� ������ (��� ������� ZP_FindSerialDevice)

#define ZP_E_INVALID_PARAM				-1	// ������������ ��������
#define ZP_E_OPEN_NOT_EXIST				-2	// ���� �� ����������
#define ZP_E_OPEN_ACCESS				-3	// ���� ����� ������ ����������
#define ZP_E_OPEN_PORT					-4	// ������ ������ �������� �����
#define ZP_E_PORT_IO_ERROR				-5	// ������ ����� (��������� �������� �� USB?)
#define ZP_E_PORT_SETUP					-6	// ������ ��������� �����
#define ZP_E_LOAD_FTD2XX				-7	// ��������� ��������� FTD2XX.DLL
#define ZP_E_INIT_SOCKET				-8	// �� ������� ���������������� ������
#define ZP_E_SERVERCLOSE				-9	// ���������� ������ �� ������� �������
#define ZP_E_NOT_ENOUGH_MEMORY			-10	// ������������ ������ ��� ��������� �������
#define ZP_E_UNSUPPORT					-11	// ������� �� ��������������
#define ZP_E_NOT_INITALIZED				-12	// �� ������������������� � ������� ZP_Initialize
#define ZP_E_CREATE_EVENT				-13	// ������ ������� CreateEvent

#define ZP_E_OTHER						-1000	// ������ ������

#define ZP_MAX_PORT_NAME				31
#define ZP_MAX_REG_DEV					32

// ZP_Initialize Flags
#define ZP_IF_NO_MSG_LOOP		0x01	// ���������� �� ����� ����� ��������� ��������� (Console or Service)
#define ZP_IF_ERROR_LOG			0x02	// ���������� � ���-���� ���������� �� ������� (%AppData%\RF Enabled\ZPort\errors.log)

#define ZP_NF_EXIST				0x01	// ZP_N_INSERT / ZP_N_REMOVE
#define ZP_NF_BUSY				0x02	// ZP_N_STATE_CHANGED
#define ZP_NF_FRIENDLY			0x04	// ZP_N_STATE_CHANGED
#define ZP_NF_WND_SYNC			0x4000	// ���������������� � �������� ��������� Windows
// (�� �������� ���� ��� ������������� ���������� ���� ZP_IF_NO_MSG_LOOP)
#define ZP_NF_ONLY_NOTIFY		0x8000	// ������ ���������� � ���������� ����� ��������� � �������
		// (��� ������������ � ��������� ��������� ����������� ������� ZP_ProcessMessages)

// ����������� ������� ZP_FindNotification (wParam - MsgCode, lParam - MsgParam)
#define ZP_N_INSERT				1	// ����������� ����� (PZP_PORT_INFO(MsgParam) - ���� � �����)
#define ZP_N_REMOVE				2	// ���������� ����� (PZP_PORT_INFO(MsgParam) - ���� � �����)
#define ZP_N_STATE_CHANGED		3	// ��������� ��������� ����� (PZP_N_CHANGE_STATE(MsgParam) - ���� �� ����������)

// ��� �����
enum ZP_PORT_TYPE
{
	ZP_PORT_UNDEF = 0,
	ZP_PORT_COM,	// Com-����
	ZP_PORT_FT,		// Ft-���� (����� ftd2xx.dll �� �/� USB)
	ZP_PORT_IP		// Ip-���� (����� TCP)
};

// ��� �����
typedef WCHAR ZP_PORT_NAME[ZP_MAX_PORT_NAME + 1];

// ���������� � �����
typedef struct _ZP_PORT_INFO
{
	ZP_PORT_TYPE nType;			// ��� �����
	ZP_PORT_NAME szName;		// ��� �����
	BOOL fBusy;					// True, ���� �����
	ZP_PORT_NAME szFriendly;	// ������������� ��� �����
	UINT nDevTypes;				// ����� ����� ���������
} *PZP_PORT_INFO;

// ��������� �������� ���������� �������
typedef struct _ZP_WAIT_SETTINGS
{
	UINT nReplyTimeout;		// ����-��� �������� ������
	INT nMaxTry;			// ���������� ������� ��������� ������
	HANDLE hCancelEvent;	// ���������� ������������ ������� Event ��� ������ �������
	UINT nReplyTimeOut0;	// ����-��� �������� ������� ������� ������
	UINT nCheckPeriod;		// ������ �������� ����� � �� (���� =0 ��� =INFINITE, �� �� RX-�������)
} *PZP_WAIT_SETTINGS;

// ���������� �� ��������� ��������� �����
typedef struct _ZP_N_CHANGE_STATE
{
	UINT nChangeMask;	// ����� ��������� (���0 Busy, ���1 Friendly)
	_ZP_PORT_INFO rInfo;
	ZP_PORT_NAME szOldFriendly;
} *PZP_N_CHANGE_STATE;

// Callback-������� (������� ��������� ������)
typedef BOOL (CALLBACK* ZP_ENUMPORTSPROC) (PZP_PORT_INFO pInfo, PVOID pUserData);
typedef BOOL (CALLBACK* ZP_NOTIFYPROC) (UINT nMsg, LPARAM nMsgParam, PVOID pUserData);

typedef struct _ZP_NOTIFY_SETTINGS
{
	UINT nNMask;		// ����� ����� ����������� ZP_NF_

	ZP_NOTIFYPROC pfnCallback;	// Callback-������� ��� �����������
	PVOID pUserData;			// �������� ������������ ��� Callback-�������

	DWORD nSDevTypes;	// ����� ����� ���������, ������������ � ����������������� �����
	DWORD nIpDevTypes;	// ����� ����� Ip-���������

	SERVICE_STATUS_HANDLE hSvcStatus;	// ���������� �������, ���������� �������� RegisterServiceCtrlHandlerEx (=NULL ���� ���������� �� �������� �������)
	UINT nCheckUsbPeriod;	// ������ �������� ��������� USB-������ (� �������������) (=0 �� ��������� 5000)
	UINT nCheckIpPeriod;	// ������ �������� ��������� IP-������ (� �������������) (=0 �� ��������� 15000)
} *PZP_NOTIFY_SETTINGS;

typedef struct _ZP_DEVICE_INFO
{
	UINT nTypeId;
	UINT nModel;
	UINT nSn;
	UINT nVersion;
} *PZP_DEVICE_INFO;

typedef BOOL (CALLBACK* ZP_ENUMDEVICEPROC) (PZP_DEVICE_INFO pInfo, PZP_PORT_INFO pPort, PVOID pUserData);
typedef BOOL (CALLBACK* ZP_DEVICEPARSEPROC) (LPCVOID pReply, INT_PTR nCount, ZP_STATUS* pError, 
	PZP_DEVICE_INFO pInfo, PZP_PORT_INFO pPort, ZP_ENUMDEVICEPROC fnEnum, PVOID pEnumParam);

typedef struct _ZP_DEVICE
{
	UINT nTypeId;					// ��� ����������
	LPCVOID pReqData;				// ������ ������� (����� ���� NULL)
	INT_PTR nReqSize;				// ���������� ���� � �������
	ZP_DEVICEPARSEPROC pfnParse;	// ������� ������� ������
	INT_PTR nDevInfoSize;			// ������ ��������� ZP_DEVICE_INFO
} *PZP_DEVICE;

typedef struct _ZP_IP_DEVICE : _ZP_DEVICE
{
	UINT nReqPort;					// ���� ��� �������
} *PZP_IP_DEVICE;

typedef struct _ZP_S_DEVICE : _ZP_DEVICE
{
	PWORD pPids;					// Pid'� USB-���������
	INT nPidCount;					// ���������� Pid'��
	UINT nBaud;						// �������� �����
	CHAR chEvent;					// ������-������� ����� �������� (���� =0, ��� �������)
	BYTE nStopBits;					// �������� ���� (ONESTOPBIT=0, ONE5STOPBITS=1, TWOSTOPBITS=2)
	LPCWSTR pszBDesc;				// �������� ����������, ��������������� ����� (DEVPKEY_Device_BusReportedDeviceDesc)
} *PZP_S_DEVICE;

typedef struct _ZP_PORT_ADDR
{
	ZP_PORT_TYPE nType;
	LPCWSTR pName;
} *PZP_PORT_ADDR;

#pragma pack() // turn byte alignment off

#ifndef ZPORT_LINKONREQUEST

// ���������� ������ ����������
ZPORT_API(DWORD) ZP_GetVersion();
ZPORT_API(ZP_STATUS) ZP_GetFtLibVersion(LPDWORD pVersion);

// �������������/����������� ����������
ZPORT_API(ZP_STATUS) ZP_Initialize(UINT nFlags);
ZPORT_API(ZP_STATUS) ZP_Finalyze();

// ������������ ���������������� ������
ZPORT_API(ZP_STATUS) ZP_EnumSerialPorts(UINT nDevTypes, ZP_ENUMPORTSPROC pEnumProc, PVOID pUserData);

// ����������� � �������� ����� (�����������/����������, ������/�����������)
ZPORT_API(ZP_STATUS) ZP_FindNotification(PHANDLE pHandle, PZP_NOTIFY_SETTINGS pSettings);
ZPORT_API(ZP_STATUS) ZP_CloseNotification(HANDLE hHandle);
ZPORT_API(ZP_STATUS) ZP_ProcessMessages(HANDLE hHandle, ZP_NOTIFYPROC pEnumProc, PVOID pUserData);
ZPORT_API(VOID) ZP_DeviceEventNotify(DWORD nEvType, PVOID pEvData);

// ������� � ������
ZPORT_API(ZP_STATUS) ZP_Open(PHANDLE pHandle, LPCWSTR szName, ZP_PORT_TYPE nType, UINT nBaud, CHAR nEvChar, BYTE nStopBits);
ZPORT_API(ZP_STATUS) ZP_Close(HANDLE hHandle);
ZPORT_API(ZP_STATUS) ZP_SetBaudAndEvChar(HANDLE hHandle, UINT nBaud, CHAR chEvChar);
ZPORT_API(ZP_STATUS) ZP_GetBaudAndEvChar(HANDLE hHandle, LPUINT pBaud, PCHAR pEvChar);
ZPORT_API(ZP_STATUS) ZP_GetFtDriverVersion(HANDLE hHandle, LPDWORD pVersion);

ZPORT_API(ZP_STATUS) ZP_Clear(HANDLE hHandle, BOOL fIn, BOOL fOut);
ZPORT_API(ZP_STATUS) ZP_Write(HANDLE hHandle, LPCVOID pBuf, INT nCount);
ZPORT_API(ZP_STATUS) ZP_Read(HANDLE hHandle, LPVOID pBuf, INT nCount, LPINT pRead);
ZPORT_API(ZP_STATUS) ZP_GetInCount(HANDLE hHandle, LPINT pCount);
ZPORT_API(ZP_STATUS) ZP_StartWaitEvent(HANDLE hHandle, PHANDLE pEvent);
ZPORT_API(ZP_STATUS) ZP_SetDtr(HANDLE hHandle, BOOL fState);
ZPORT_API(ZP_STATUS) ZP_SetRts(HANDLE hHandle, BOOL fState);

// ������ � ������������
ZPORT_API(ZP_STATUS) ZP_RegSerialDevice(PZP_S_DEVICE pParams);
ZPORT_API(ZP_STATUS) ZP_EnumSerialDevices(DWORD nTypeMask, PZP_PORT_ADDR pPorts, INT nPCount, 
	ZP_ENUMDEVICEPROC pEnumProc, PVOID pUserData, PZP_WAIT_SETTINGS pWait=NULL);
ZPORT_API(ZP_STATUS) ZP_FindSerialDevice(DWORD nTypeMask, PZP_PORT_ADDR pPorts, INT nPCount, 
	PZP_DEVICE_INFO pInfo, INT_PTR nInfoSize, PZP_PORT_INFO pPort, PZP_WAIT_SETTINGS pWait=NULL);
ZPORT_API(ZP_STATUS) ZP_RegIpDevice(PZP_IP_DEVICE pParams);
ZPORT_API(ZP_STATUS) ZP_EnumIpDevices(DWORD nTypeMask, ZP_ENUMDEVICEPROC pEnumProc, PVOID pUserData, PZP_WAIT_SETTINGS pWait=NULL);

#endif
