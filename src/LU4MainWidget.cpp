/*!
 *\file LU4MainWidget.cpp
 *\brief This file contains LU4MainWidget class declaration
 */

#include <QGridLayout>
#include <QLabel>
#include <QPixmap>
#include <QTime>
#include <QTimer>

#include "LU4ListViewWidget.h"
#include "LU4Reader.h"

#include "LU4MainWidget.h"
#include "ui_LU4MainWidget.h"


LU4MainWidget::LU4MainWidget(QWidget* parent /*= NULL*/)
  : QWidget(parent),
    mUi(new Ui::LU4MainWidgetForm())
{
  mUi->setupUi(this);

  mColonVisible = true;
  QTimer* timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
  timer->singleShot(0, this, SLOT(updateTime()));
  timer->start(1000);

  QPixmap logo("../res/logo.png");
  logo = logo.scaledToHeight(80);
  mUi->mLogoLabel->setPixmap(logo);

  mUi->mTabWidget->setTabPosition(QTabWidget::North);

  mList0 = new LU4ListViewWidget(this);
  mList1 = new LU4ListViewWidget(this);

  mUi->mTabWidget->addTab(mList0, tr("First"));
  mUi->mTabWidget->addTab(mList1, tr("Second"));

  mReader0 = new LU4ZGuardReader(ZP_PORT_COM, L"COM3", 1);
  mReader0->connect();

  auto connection = connect(mReader0, SIGNAL(swipeCard(const QString&)), mList0, SLOT(onSwipeCard(const QString&)));
  Q_ASSERT(connection);
}

LU4MainWidget::~LU4MainWidget()
{

}

void LU4MainWidget::updateTime()
{
  QTime time = QTime::currentTime();
  QString text;
  
  if (mColonVisible)
  {
    text = time.toString("hh:mm");
  }
  else
  {
    text = time.toString("hh mm");
  }
  mColonVisible = !mColonVisible;
  
  mUi->lcdNumber->display(text);
  //mUi->lcdNumber->setDigitCount(10);
}

