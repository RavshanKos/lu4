/*!
 *\file LU4ListViewItem.cpp
 *\brief This file contains LU4ListViewItem class declaration
 */

#include <QApplication>
#include <QClipboard>

#include "LU4ListViewItemWidget.h"
#include "LU4ListViewItem.h"


namespace Uniforms
{
  const QColor gColorNew = QColor(255, 128, 0, 255);
  const QColor gColorOld = QColor(200, 200, 200, 255);
  const int    gItemHeight = 40;
}


LU4ListViewItem::LU4ListViewItem(const QString& name, bool isNew, QListWidget* parent)
  : QListWidgetItem(parent),
    mWidget(new LU4ListViewItemWidget(name, parent)),
    mName(name),
    mIsNew(isNew)
{
  //setSizeHint(QSize(parent->width(), Uniforms::gItemHeight));
  setSizeHint(QSize(mWidget->width(), mWidget->height()));
  parent->setItemWidget(this, mWidget);
  updateColor();

  // Connect copy signal of widget to copy slot
  auto connection = connect(mWidget, SIGNAL(copy()), this, SLOT(onCopy()));
  Q_ASSERT(connection);
}

LU4ListViewItem::~LU4ListViewItem()
{

}

void LU4ListViewItem::setNew(bool isNew /*= true*/)
{
  mIsNew = isNew;
  updateColor();
}

bool LU4ListViewItem::getNew() const
{
  return mIsNew;
}

void LU4ListViewItem::updateColor()
{
  if (mIsNew)
  {
    setBackgroundColor(Uniforms::gColorNew);
  }
  else
  {
    setBackgroundColor(Uniforms::gColorOld);
  }
}

void LU4ListViewItem::onCopy()
{
  // Get clipboard instance
  QClipboard *clipboard = QApplication::clipboard();
  // Copy item name
  clipboard->setText(mName);
  // Mark item as old
  setNew(false);
}


