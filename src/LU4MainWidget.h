/*!
 *\file LU4MainWidget.h
 *\brief This file contains LU4MainWidget class declaration
 */

#ifndef LU4_MAIN_WIDGET_H
  #define LU4_MAIN_WIDGET_H

  #include <QScopedPointer>
  #include <QTabWidget>

  #include "LU4.h"

  class LU4ListViewWidget;
  class LU4ZGuardReader;
  namespace Ui
  {
    class LU4MainWidgetForm;
  }

  /*!
   *\class LU4MainWidget
   *\brief Main application widget
   */
  class LU4MainWidget : public QWidget
  {
    Q_OBJECT
  public:
    /*!
     *\brief Constructor
     *\param parent Parent widget
     */
    explicit LU4MainWidget(QWidget* parent = nullptr);
    /*!
     *\brief Destructor
     */
    virtual ~LU4MainWidget() OVERRIDE;

  private slots:
    void updateTime();

  private:
    //! Ui
    QScopedPointer<Ui::LU4MainWidgetForm>   mUi;
    //! First list
    LU4ListViewWidget*                      mList0;
    //! Second list
    LU4ListViewWidget*                      mList1;
    //! Z-Guard reader
    LU4ZGuardReader*                        mReader0;
    //! Colon visible flag
    bool                                    mColonVisible;
  };

#endif // LU4_MAIN_WIDGET_H