/*!
 *\file LU4ListViewWidget.h
 *\brief This file contains LU4ListViewItemWidget class declaration
 */

#ifndef LU4_LIST_VIEW_ITEM_WIDGET_H
  #define LU4_LIST_VIEW_ITEM_WIDGET_H

  #include <QScopedPointer>
  #include <QWidget>

  #include "LU4.h"

  namespace Ui
  {
    class LU4ListViewItemWidgetForm;
  }

  /*!
   *\class LU4ListViewItemWidget
   *\brief Widget for list view item
   */
  class LU4ListViewItemWidget : public QWidget
  {
    Q_OBJECT
  public:
    /*!
     *\brief Constructor
     */
    explicit LU4ListViewItemWidget(const QString& name, QWidget* parent = nullptr);
    /*!
     *\brief Destructor
     */
    virtual ~LU4ListViewItemWidget() OVERRIDE;

  signals:
    void copy();

  private slots:
    void onCopyAction();

  private:
    //! Ui
    QScopedPointer<Ui::LU4ListViewItemWidgetForm> mUi;
    //! Item name
    QString                                       mName;
  };

#endif // LU4_LIST_VIEW_ITEM_WIDGET_H