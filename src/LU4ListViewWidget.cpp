/*!
 *\file LU4ListViewWidget.cpp
 *\brief This file contains LU4ListViewWidget class declaration
 */

#include <QGridLayout>
#include <QLabel>

#include "LU4ListViewItem.h"
#include "LU4ListViewWidget.h"


LU4ListViewWidget::LU4ListViewWidget(QWidget* parent /*= NULL*/)
  : QListWidget(parent)
{
  addItem("123", true);
  addItem("231", false);

  connect(this, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
          this, SLOT(onCurrentItemChanged(QListWidgetItem*, QListWidgetItem*)));
}

LU4ListViewWidget::~LU4ListViewWidget()
{

}

void LU4ListViewWidget::onSwipeCard(const QString& id)
{
  addItem(id, true);
}


void LU4ListViewWidget::onCurrentItemChanged(QListWidgetItem* current, QListWidgetItem* privious)
{
  //LU4ListViewItemWidget* item = static_cast<LU4ListViewItemWidget*>(itemWidget(current));
  LU4ListViewItem* item = static_cast<LU4ListViewItem*>(current);

  //item->setNew(!item->getNew());
}

void LU4ListViewWidget::addItem(const QString& name, bool isNew /*= true*/)
{
  LU4ListViewItem* item = new LU4ListViewItem(name, isNew, this);
  QListWidget::addItem(item);
}

