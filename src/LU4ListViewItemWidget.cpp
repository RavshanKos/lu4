/*!
 *\file LU4ListViewWidget.cpp
 *\brief This file contains LU4ListViewWidget class declaration
 */

#include <QLabel>
#include <QToolButton>

#include "LU4ListViewItemWidget.h"
#include "ui_LU4ListViewItemWidget.h"


LU4ListViewItemWidget::LU4ListViewItemWidget(const QString& name, QWidget* parent /*= nullptr*/)
  : mUi(new Ui::LU4ListViewItemWidgetForm())
{
  mUi->setupUi(this);

  mUi->mTextLabel->setText(name);
  //setBackgroundRole(QPalette::Shadow);

  QAction* copyAction = new QAction(tr("Copy"), this);
  copyAction->setToolTip(tr("Copy"));
  copyAction->setShortcut(QKeySequence(QKeySequence::Print));
  copyAction->setIcon(QIcon(QIcon("../res/Basic-Copy-icon.png")));
  mUi->mCopyToolButton->setDefaultAction(copyAction);
  auto connection = connect(copyAction, SIGNAL(triggered(bool)), this, SLOT(onCopyAction()));
  Q_ASSERT(connection);
}

LU4ListViewItemWidget::~LU4ListViewItemWidget()
{

}

void LU4ListViewItemWidget::onCopyAction()
{
  emit copy();
}
