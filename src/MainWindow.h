/*!
 *\file MainWindow.h
 *\brief This file contains application main window definition
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

  #include <QMainWindow>

  #include "LU4.h"
  
  class LU4MainWidget;

  /*!
   *\class MainWindow
   *\brief This class represents application main window, that holds all toolbars, menus and dock widgets
   *\      Main window doesn't have parent.
   */
  class MainWindow : public QMainWindow
  {
    Q_OBJECT
  public:
    /*!
      *\brief Constructor
      */
    explicit MainWindow();

    /*!
      *\brief Destructor
      */
    virtual ~MainWindow() OVERRIDE;

    /*!
      *\brief Perform main window initialization
      *\param argc Number of command line arguments passed to the application
      *\param argv Command line arguments strings
      */
    void initialize(int argc, char *argv[]);

  private:
    /*!
      *\brief Handle closeEvent
      *\param event Event
      */
    virtual void closeEvent(QCloseEvent* event) OVERRIDE;
    /*!
      *\brief Handle mousePressEvent
      *\param event Event
      */
    virtual void mousePressEvent(QMouseEvent* event) OVERRIDE;
    /*!
      *\brief Handle mouseReleaseEvent
      *\param event Event
      */
    virtual void mouseReleaseEvent(QMouseEvent* event) OVERRIDE;
    /*!
      *\brief Handle mouseDoubleClickEvent
      *\param event Event
      */
    virtual void mouseDoubleClickEvent(QMouseEvent* event) OVERRIDE;
    /*!
      *\brief Handle mouseMoveEvent
      *\param event Event
      */
    virtual void mouseMoveEvent(QMouseEvent* event) OVERRIDE;
    /*!
      *\brief Handle wheelEvent
      *\param event Event
      */
    virtual void wheelEvent(QWheelEvent* event) OVERRIDE;
    /*!
      *\brief Handle keyPressEvent
      *\param event Event
      */
    virtual void keyPressEvent(QKeyEvent* event) OVERRIDE;
    /*!
      *\brief Handle keyReleaseEvent
      *\param event Event
      */
    virtual void keyReleaseEvent(QKeyEvent* event) OVERRIDE;

  private:
    //! Central widget
    LU4MainWidget* mMainWidget;
  };

#endif // MAINWINDOW_H